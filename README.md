# Chat Challenge

Here is my attempt at the chat app challenge. My assumptions;
- Default user "Bob" is the author
- Able to switch to another user "Alice" to emulate received messages
  - Can do this using overflow menu
  - Clear chat using overflow menu
- To test time related requirements I assume one will alter the device time settings.

## Architecture
I decided to try and use the clean architecture approach to structure my solution. Split into the following layer;
- Data
  - I am exposing my repository through an interface to follow the SOLID principles.
- Domain 
  - I am using use cases to separate complex logic. It will also help scale the app in the future.
- Presentation
  - For this layer I took the approach of using MVVM structure, along with usecases in the Domain layer.

## Frameworks
- Coroutines
  - I am using Flows to provide and update data from my local cache to view models
- Room
  - Operations are handled through the Dao and provided using DI to cache repository class, for example it provides messages in order of the time stored.
- Jetpack Compose
  - Using components to create a single Activity app and using techniques like state hoisting and listener patterns I have attempted to write clean compose code. 
  - I am still learning a lot about this and hope to learn more of its features to utilise it to the fullest.
- Hilt
  - DI to provide cache repository and Dao. Simple use-case of providing singletons.

## Testing
Unit testing on usecases
- Clear messages test
- Save messages tests
- Getting all messages
  - Tests can be found in the usecase test package
    - Test when no messages
    - Test when there one message
    - Test when two messages from same author sent under 20 seconds
    - Test when two messages each from different author
    - Test when two messages sent from same user 
    - Test when two messages sent from the same user are more than 20 seconds apart
    - Test when two messages are over an hour apart
  - Tests will check if messages have a tail and sections are placed correctly.

## Improvements
Given more time;
- Add instrumented tests
- Can utilise MaterialTheme to set values, currently using default.
- Can refactor Color references in component to use values set in MaterialTheme
- Refactor MessageItem to be a builder so code can be cleaner in GetMessagesUseCase
- Can improve UI with animations
- Improve UI in general
  - Send button can provide visual feedback
  - Sending an empty message can propagate an error to uiState; in order to display an error.
- Create index for chat item and assign key on LazyColumn to improve ui recompositions.

## Video + Screenshot
![Chat demo](media/chat_app_test.webm)

*Theme update*

<img src="media/Screenshot_dark.png" alt= "" width="360" height="740"/>