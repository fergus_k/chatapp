package uk.co.chatchallenge.usecase

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import uk.co.chatchallenge.domain.model.MessageModel
import uk.co.chatchallenge.domain.model.ui.MessageItem
import uk.co.chatchallenge.domain.model.ui.SectionItem
import uk.co.chatchallenge.domain.repo.CacheRepository
import uk.co.chatchallenge.domain.usecase.GetAllMessagesUseCase

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(MockitoJUnitRunner::class)
class GetAllMessagesUseCaseTest {

    @Mock
    private lateinit var cache: CacheRepository

    private lateinit var useCaseTest: GetAllMessagesUseCase


    @Before
    fun setup() {
        useCaseTest = GetAllMessagesUseCase(cache)
    }

    @Test
    fun testNoMessages() = runTest {

        val mockFlow = flowOf(emptyList<MessageModel>())
        whenever(cache.getAllMessagesSortedByTime()).thenReturn(mockFlow)

        val resultFlow = useCaseTest.execute(CURRENT_USER)

        verify(cache, times(1)).getAllMessagesSortedByTime()
        assertEquals(0, resultFlow.first().size)

    }

    @Test
    fun testOneMessage() = runTest {

        val mockFlow = flowOf(ONE_ITEM_SCENARIO)

        whenever(cache.getAllMessagesSortedByTime()).thenReturn(mockFlow)

        val resultFlow = useCaseTest.execute(CURRENT_USER)

        verify(cache, times(1)).getAllMessagesSortedByTime()
        resultFlow.first().let {

            assertEquals(2, it.size)

            (it[0] as SectionItem).run {
                assertTrue(title.isNotEmpty())
            }

            (it[1] as MessageItem).run {
                assertEquals("Hello!", text)
                assertEquals(false, isAuthor)
                assertEquals(true, requireTail)
            }
        }
    }

    @Test
    fun testMultipleMessageLessThanTwentySeconds() = runTest {
        val mockFlow = flowOf(TWO_ITEM_SCENARIO)

        whenever(cache.getAllMessagesSortedByTime()).thenReturn(mockFlow)

        val resultFlow = useCaseTest.execute(CURRENT_USER)

        verify(cache, times(1)).getAllMessagesSortedByTime()
        resultFlow.first().let {

            assertEquals(3, it.size)

            (it[0] as SectionItem).run {
                assertTrue(title.isNotEmpty())
            }

            (it[1] as MessageItem).run {
                assertEquals("Hello!", text)
                assertEquals(false, isAuthor)
                assertEquals(false, requireTail)
            }

            (it[2] as MessageItem).run {
                assertEquals("Hello!", text)
                assertEquals(false, isAuthor)
                assertEquals(true, requireTail)
            }
        }
    }

    @Test
    fun testMultipleMessageDifferentUserAfter() = runTest {
        val mockFlow = flowOf(TWO_ITEM_AUTHOR_TEST)

        whenever(cache.getAllMessagesSortedByTime()).thenReturn(mockFlow)

        val resultFlow = useCaseTest.execute(CURRENT_USER)

        verify(cache, times(1)).getAllMessagesSortedByTime()
        resultFlow.first().let {

            assertEquals(3, it.size)

            (it[0] as SectionItem).run {
                assertTrue(title.isNotEmpty())
            }

            (it[1] as MessageItem).run {
                assertEquals("Hello!", text)
                assertEquals(true, isAuthor)
                assertEquals(true, requireTail)
            }

            (it[2] as MessageItem).run {
                assertEquals("Hello!", text)
                assertEquals(false, isAuthor)
                assertEquals(true, requireTail)
            }
        }
    }

    @Test
    fun testMessageSentAfterTwentySeconds() = runTest {
        val mockFlow = flowOf(OVER_TWENTY_SCENARIO)

        whenever(cache.getAllMessagesSortedByTime()).thenReturn(mockFlow)

        val resultFlow = useCaseTest.execute(CURRENT_USER)

        verify(cache, times(1)).getAllMessagesSortedByTime()
        resultFlow.first().let {

            assertEquals(3, it.size)

            (it[0] as SectionItem).run {
                assertTrue(title.isNotEmpty())
            }

            (it[1] as MessageItem).run {
                assertEquals("Hello!", text)
                assertEquals(true, isAuthor)
                assertEquals(true, requireTail)
            }

            (it[2] as MessageItem).run {
                assertEquals("Hello!", text)
                assertEquals(true, isAuthor)
                assertEquals(true, requireTail)
            }
        }
    }

    @Test
    fun testMessageSentAfterOneHour() = runTest {
        val mockFlow = flowOf(OVER_HOUR_SCENARIO)

        whenever(cache.getAllMessagesSortedByTime()).thenReturn(mockFlow)

        val resultFlow = useCaseTest.execute(CURRENT_USER)

        verify(cache, times(1)).getAllMessagesSortedByTime()
        resultFlow.first().let {

            assertEquals(4, it.size)

            (it[0] as SectionItem).run {
                assertTrue(title.isNotEmpty())
            }

            (it[1] as MessageItem).run {
                assertEquals("Hello!", text)
                assertEquals(true, isAuthor)
                assertEquals(true, requireTail)
            }

            (it[2] as SectionItem).run {
                assertTrue(title.isNotEmpty())
            }

            (it[3] as MessageItem).run {
                assertEquals("Hello!", text)
                assertEquals(true, isAuthor)
                assertEquals(true, requireTail)
            }
        }
    }

    companion object {
        private const val CURRENT_USER = "Test"
        private val ONE_ITEM_SCENARIO = listOf(
            MessageModel("Hello!", "Alice", 10000)
        )
        private val TWO_ITEM_SCENARIO = listOf(
            MessageModel("Hello!", "Alice", 10000),
            MessageModel("Hello!", "Alice", 10010)
        )
        private val TWO_ITEM_AUTHOR_TEST = listOf(
            MessageModel("Hello!", "Test", 10000),
            MessageModel("Hello!", "Alice", 10010)
        )
        private val OVER_TWENTY_SCENARIO = listOf(
            MessageModel("Hello!", "Test", 10000),
            MessageModel("Hello!", "Test", 50000)
        )
        private val OVER_HOUR_SCENARIO = listOf(
            MessageModel("Hello!", "Test", 10_000),
            MessageModel("Hello!", "Test", 10_000_000)
        )
    }
}