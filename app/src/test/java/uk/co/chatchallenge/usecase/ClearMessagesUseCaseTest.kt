package uk.co.chatchallenge.usecase

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import uk.co.chatchallenge.domain.repo.CacheRepository
import uk.co.chatchallenge.domain.usecase.ClearAllMessagesUseCase

@RunWith(MockitoJUnitRunner::class)
class ClearMessagesUseCaseTest {

    @Mock
    private lateinit var cache: CacheRepository

    private lateinit var useCaseTest: ClearAllMessagesUseCase

    @Before
    fun setup() {
        useCaseTest = ClearAllMessagesUseCase(cache)
    }

    @Test
    fun testMessagesCleared() {
        useCaseTest.execute()
        verify(cache, times(1)).clearAllMessages()
    }
}