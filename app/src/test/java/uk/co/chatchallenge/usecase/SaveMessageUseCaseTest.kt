package uk.co.chatchallenge.usecase

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.any
import org.mockito.kotlin.argumentCaptor
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import uk.co.chatchallenge.domain.model.MessageModel
import uk.co.chatchallenge.domain.repo.CacheRepository
import uk.co.chatchallenge.domain.usecase.SaveMessageUseCase

@RunWith(MockitoJUnitRunner::class)
class SaveMessageUseCaseTest {

    @Mock
    private lateinit var cache: CacheRepository

    private lateinit var useCaseTest: SaveMessageUseCase

    @Before
    fun setup() {
        useCaseTest = SaveMessageUseCase(cache)
    }

    @Test
    fun testBlankMessage() {
        useCaseTest.execute(user = DEFAULT_USER, text = "")
        verify(cache, times(0)).saveMessage(any())
    }

    @Test
    fun testMessageSuccess() {
        val argCaptor = argumentCaptor<MessageModel>()
        useCaseTest.execute(user = DEFAULT_USER, text = DEFAULT_MESSAGE)
        verify(cache, times(1)).saveMessage(argCaptor.capture())

        argCaptor.lastValue.let {
            assertEquals(DEFAULT_MESSAGE, it.text)
            assertEquals(DEFAULT_USER, it.username)
        }
    }

    companion object {
        const val DEFAULT_USER = "Test"
        const val DEFAULT_MESSAGE = "Hello world!"
    }

}