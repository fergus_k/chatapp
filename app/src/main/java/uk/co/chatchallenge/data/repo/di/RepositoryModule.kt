package uk.co.chatchallenge.data.repo.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import uk.co.chatchallenge.data.repo.RoomCacheRepositoryImpl
import uk.co.chatchallenge.domain.repo.CacheRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun provideCacheRepository(repository: RoomCacheRepositoryImpl): CacheRepository
}