package uk.co.chatchallenge.data.repo

import kotlinx.coroutines.flow.Flow
import uk.co.chatchallenge.data.local.MessageDao
import uk.co.chatchallenge.data.model.MessageEntity
import uk.co.chatchallenge.data.model.convert
import uk.co.chatchallenge.domain.model.MessageModel
import uk.co.chatchallenge.domain.repo.CacheRepository
import javax.inject.Inject

class RoomCacheRepositoryImpl @Inject constructor(private val messageDao: MessageDao)
    : CacheRepository {

    override fun getAllMessagesSortedByTime(): Flow<List<MessageModel>> {
        return messageDao.getMessages().convert()
    }

    override fun clearAllMessages() {
        messageDao.deleteAllRows()
    }

    override fun saveMessage(message: MessageModel) {
        messageDao.putMessage(
            MessageEntity(userName = message.username,
                text = message.text,
                sendTime = message.sendTimeInMillis)
        )
    }
}