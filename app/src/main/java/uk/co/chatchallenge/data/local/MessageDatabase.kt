package uk.co.chatchallenge.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import uk.co.chatchallenge.data.model.MessageEntity

@Database(entities = [MessageEntity::class], version = 1)
abstract class MessageDatabase: RoomDatabase() {

    abstract fun getMessageDao(): MessageDao
}