package uk.co.chatchallenge.util

import android.annotation.SuppressLint
import uk.co.chatchallenge.domain.model.ui.MessageDate
import java.text.SimpleDateFormat
import java.util.*


@SuppressLint("SimpleDateFormat")
fun Long.toDisplayableDate(): MessageDate {
    val date = Date(this)
    return SimpleDateFormat("EEEE,HH,mm").also {
        it.timeZone = TimeZone.getTimeZone("UTC")
    }.format(date).split(",").takeIf { it.size == 3 }?.let {
        MessageDate(day = it[0], hour = it[1], minute = it[2])
    } ?: MessageDate("", "", "")
}