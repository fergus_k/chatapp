package uk.co.chatchallenge.util

val SENDER = TestUser.BOB
const val CLEAR_OPTION = "Clear"

enum class TestUser(val text: String) {
    BOB("Bob"),
    ALICE("Alice");

    companion object {
        private val values = values().associateBy {
            it.text
        }

        fun fromString(text: String): TestUser {
            return values[text] ?: BOB
        }
    }
}