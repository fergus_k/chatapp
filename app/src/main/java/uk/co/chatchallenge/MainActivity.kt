package uk.co.chatchallenge

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.getValue
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import dagger.hilt.android.AndroidEntryPoint
import uk.co.chatchallenge.presentation.home.ChatViewModel
import uk.co.chatchallenge.presentation.home.ChatWindowComponent
import uk.co.chatchallenge.presentation.theme.ChatChallengeTheme
import uk.co.chatchallenge.util.CLEAR_OPTION

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val viewModel: ChatViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            ChatChallengeTheme {

                val uiState by viewModel.uiState.collectAsStateWithLifecycle()

                Surface(
                    color = MaterialTheme.colors.background,
                ) {
                    ChatWindowComponent(
                        user = uiState.user.text,
                        messages = uiState.chatItems,
                        onMessageSend = {
                            viewModel.sendMessage(it)
                        }, onMenuItemSelected = {
                            when (it) {
                                CLEAR_OPTION -> viewModel.clearChat()
                                else -> viewModel.setUser(it)
                            }

                        })
                }
            }
        }
    }
}