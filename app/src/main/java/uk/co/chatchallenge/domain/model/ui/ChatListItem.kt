package uk.co.chatchallenge.domain.model.ui

sealed class ChatItem
data class SectionItem(val title: String): ChatItem()
data class MessageItem(val text: String,
                       val isAuthor: Boolean,
                       var requireTail: Boolean): ChatItem()