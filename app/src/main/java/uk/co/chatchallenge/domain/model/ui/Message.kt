package uk.co.chatchallenge.domain.model.ui

data class Message(val text: String,
                   val metadata: MessageMetadata)
