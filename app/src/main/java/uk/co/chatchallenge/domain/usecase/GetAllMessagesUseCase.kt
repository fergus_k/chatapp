package uk.co.chatchallenge.domain.usecase

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import uk.co.chatchallenge.domain.model.convert
import uk.co.chatchallenge.domain.model.ui.ChatItem
import uk.co.chatchallenge.domain.model.ui.Message
import uk.co.chatchallenge.domain.model.ui.MessageItem
import uk.co.chatchallenge.domain.model.ui.SectionItem
import uk.co.chatchallenge.domain.repo.CacheRepository
import javax.inject.Inject

class GetAllMessagesUseCase @Inject constructor(private val cache: CacheRepository) {

    fun execute(currentUser: String): Flow<List<ChatItem>> {
        return cache.getAllMessagesSortedByTime().convert().map {
            it.convert(currentUser)
        }
    }

    private fun List<Message>.convert(currentUser: String): List<ChatItem> {

        val chatList = mutableListOf<ChatItem>()

        forEachIndexed { index, message ->
            val previousMessage = this.getOrNull(index - 1)
            val nextMessage = this.getOrNull(index + 1)

            var hasTail = false

            if (previousMessage == null ||
                message.calculateTimeDiff(previousMessage) > HOUR_IN_MILLIS
            ) {
                chatList.add(
                    SectionItem(message.metadata.displayDate.toString())
                )
            }

            nextMessage?.let {
                if (it.metadata.userName != message.metadata.userName) hasTail = true
                if (it.calculateTimeDiff(message) > TWENTY_SECONDS_IN_MILLIS) hasTail = true
            }

            if (index == lastIndex) hasTail = true

            chatList.add(
                MessageItem(message.text, message.metadata.userName == currentUser, hasTail)
            )
        }

        return chatList
    }

    private fun Message.calculateTimeDiff(other: Message): Long {
        return this.metadata.time - other.metadata.time
    }

    companion object {
        private const val HOUR_IN_MILLIS = 3_600_000
        private const val TWENTY_SECONDS_IN_MILLIS = 20_000
    }
}