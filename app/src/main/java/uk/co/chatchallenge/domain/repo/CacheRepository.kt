package uk.co.chatchallenge.domain.repo

import kotlinx.coroutines.flow.Flow
import uk.co.chatchallenge.domain.model.MessageModel

interface CacheRepository {

    fun getAllMessagesSortedByTime(): Flow<List<MessageModel>>

    fun clearAllMessages()

    fun saveMessage(message: MessageModel)
}