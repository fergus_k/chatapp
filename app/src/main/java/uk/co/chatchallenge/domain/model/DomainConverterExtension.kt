package uk.co.chatchallenge.domain.model

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import uk.co.chatchallenge.domain.model.ui.Message
import uk.co.chatchallenge.domain.model.ui.MessageMetadata
import uk.co.chatchallenge.util.toDisplayableDate

fun MessageModel.convert(): Message {
    return Message(
        text = text,
        metadata = MessageMetadata(
            time = sendTimeInMillis,
            displayDate = sendTimeInMillis.toDisplayableDate(),
            userName = username
        )
    )
}

fun Flow<List<MessageModel>>.convert(): Flow<List<Message>> {
    return map { flow -> flow.map { it.convert() } }
}