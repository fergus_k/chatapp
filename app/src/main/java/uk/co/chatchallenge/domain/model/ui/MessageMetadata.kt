package uk.co.chatchallenge.domain.model.ui

data class MessageMetadata(
    val time: Long,
    val displayDate: MessageDate,
    val userName: String
)