package uk.co.chatchallenge.domain.usecase

import uk.co.chatchallenge.domain.model.MessageModel
import uk.co.chatchallenge.domain.repo.CacheRepository
import javax.inject.Inject

class SaveMessageUseCase @Inject constructor(private val cache: CacheRepository) {

    fun execute(user: String, text: String) {
        text.takeIf { it.isNotBlank() }?.let {
            cache.saveMessage(
                MessageModel(
                    text = text,
                    username = user,
                    sendTimeInMillis = System.currentTimeMillis()
                )
            )
        } ?: kotlin.run {
            // propagate error to ui state.
        }
    }
}