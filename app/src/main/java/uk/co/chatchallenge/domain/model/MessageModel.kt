package uk.co.chatchallenge.domain.model

data class MessageModel(
    val text: String,
    val username: String,
    val sendTimeInMillis: Long
)
