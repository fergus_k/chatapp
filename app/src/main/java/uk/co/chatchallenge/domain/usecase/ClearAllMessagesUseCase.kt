package uk.co.chatchallenge.domain.usecase

import uk.co.chatchallenge.domain.repo.CacheRepository
import javax.inject.Inject

class ClearAllMessagesUseCase @Inject constructor(private val cache: CacheRepository) {

    fun execute() {
        cache.clearAllMessages()
    }
}