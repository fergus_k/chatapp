package uk.co.chatchallenge.presentation.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import uk.co.chatchallenge.domain.usecase.ClearAllMessagesUseCase
import uk.co.chatchallenge.domain.usecase.GetAllMessagesUseCase
import uk.co.chatchallenge.domain.usecase.SaveMessageUseCase
import uk.co.chatchallenge.util.SENDER
import uk.co.chatchallenge.util.TestUser
import javax.inject.Inject

@HiltViewModel
class ChatViewModel @Inject constructor(
    private val getAllMessagesUseCase: GetAllMessagesUseCase,
    private val saveMessageUseCase: SaveMessageUseCase,
    private val clearAllMessagesUseCase: ClearAllMessagesUseCase
) : ViewModel() {

    private val _user = MutableStateFlow(SENDER)

    val uiState: StateFlow<ChatUiState> = getAllMessagesUseCase.execute(SENDER.text)
        .combine(_user) { items, testUser ->
            ChatUiState(chatItems = items, user = testUser)
        }.stateIn(viewModelScope, SharingStarted.Eagerly, ChatUiState())

    fun setUser(user: String) {
        _user.value = TestUser.fromString(user)
    }

    fun sendMessage(text: String) {
        viewModelScope.launch(Dispatchers.IO) {
            saveMessageUseCase.execute(_user.value.text, text)
        }
    }

    fun clearChat() {
        viewModelScope.launch(Dispatchers.IO) {
            clearAllMessagesUseCase.execute()
        }
    }

}