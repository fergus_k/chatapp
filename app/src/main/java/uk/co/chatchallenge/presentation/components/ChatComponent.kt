package uk.co.chatchallenge.presentation.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import uk.co.chatchallenge.domain.model.ui.ChatItem
import uk.co.chatchallenge.domain.model.ui.MessageItem
import uk.co.chatchallenge.domain.model.ui.SectionItem

@Composable
fun ChatComponent(
    chatListItems: List<ChatItem>,
    listState: LazyListState,
    contentPadding: PaddingValues
) {

    Surface(
        modifier = Modifier.padding(bottom = contentPadding.calculateBottomPadding())
    ) {
        LazyColumn(
            state = listState,
            reverseLayout = true,
            verticalArrangement = Arrangement.spacedBy(8.dp),
        ) {

            items(items = chatListItems.asReversed()) { chatItem ->
                when (chatItem) {
                    is MessageItem -> {
                        MessageBubble(
                            text = chatItem.text,
                            isAuthor = chatItem.isAuthor,
                            hasTail = chatItem.requireTail,
                            modifier = Modifier.defaultMinSize(minWidth = 30.dp)
                        )
                    }
                    is SectionItem -> {
                        SectionItemComponent(title = chatItem.title)
                    }
                }
            }

            item {
                SectionItemComponent("This is the start of your chat!")
            }
        }
    }

}