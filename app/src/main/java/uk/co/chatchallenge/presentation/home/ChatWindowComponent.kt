package uk.co.chatchallenge.presentation.home

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import uk.co.chatchallenge.domain.model.ui.ChatItem
import uk.co.chatchallenge.presentation.components.ChatComponent
import uk.co.chatchallenge.presentation.components.MessageInputBarComponent
import uk.co.chatchallenge.util.TestUser

@Composable
fun ChatWindowComponent(
    user: String,
    messages: List<ChatItem>,
    onMessageSend: (String) -> Unit,
    onMenuItemSelected: (String) -> Unit
) {

    val chatListState = rememberLazyListState()
    var chatMenuState by remember { mutableStateOf(false) }

    LaunchedEffect(key1 = messages.size) {
        chatListState.animateScrollToItem(0)
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = "Chat as $user")
                },
                actions = {
                    IconButton(onClick = { chatMenuState = !chatMenuState }) {
                        Icon(Icons.Default.MoreVert, contentDescription = "Options")
                    }
                    DropdownMenu(
                        expanded = chatMenuState,
                        onDismissRequest = {
                            chatMenuState = false
                        }) {
                        DropdownMenuItem(onClick = {
                            onMenuItemSelected(TestUser.ALICE.text)
                            chatMenuState = false
                        }) {
                            Text(text = TestUser.ALICE.text)
                        }
                        DropdownMenuItem(onClick = {
                            onMenuItemSelected(TestUser.BOB.text)
                            chatMenuState = false
                        }) {
                            Text(text = TestUser.BOB.text)
                        }
                        DropdownMenuItem(onClick = {
                            onMenuItemSelected("Clear")
                            chatMenuState = false
                        }) {
                            Text(text = "Clear chat")
                        }
                    }
                }
            )
        },
        bottomBar = {
            Surface {
                MessageInputBarComponent(
                    modifier = Modifier
                        .background(color = MaterialTheme.colors.surface, shape = RoundedCornerShape(16.dp))
                        .padding(top = 10.dp, bottom = 10.dp)
                        .fillMaxWidth(),
                    onSendPressed = {
                        onMessageSend(it)
                    }
                )
            }
        }
    ) { paddingValues ->

        ChatComponent(
            chatListItems = messages,
            listState = chatListState,
            contentPadding = paddingValues
        )
    }
}