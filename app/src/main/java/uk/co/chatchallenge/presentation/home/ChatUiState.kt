package uk.co.chatchallenge.presentation.home

import uk.co.chatchallenge.domain.model.ui.ChatItem
import uk.co.chatchallenge.util.TestUser

data class ChatUiState(
    val chatItems: List<ChatItem> = emptyList(),
    val user: TestUser = TestUser.BOB
)