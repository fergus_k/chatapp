package uk.co.chatchallenge.presentation.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun MessageBubble(
    text: String,
    isAuthor: Boolean,
    modifier: Modifier = Modifier,
    hasTail: Boolean = false
) {
    val bubbleColor = if (isAuthor) Color.Red else Color.Gray
    val contentColor = if (isAuthor) Color.White else Color.Black
    val arrangement = if (isAuthor) Arrangement.End else Arrangement.Start

    val padStart = if (isAuthor) 30.dp else 10.dp
    val padEnd = if (isAuthor) 10.dp else 30.dp

    val shape = if (isAuthor)
        RoundedCornerShape(16.dp, 16.dp, if (hasTail) 0.dp else 16.dp, 16.dp)
    else RoundedCornerShape(16.dp, 16.dp, 16.dp, if (hasTail) 0.dp else 16.dp)

    Row(modifier = Modifier.fillMaxWidth().padding(end = padEnd, start = padStart),
        horizontalArrangement = arrangement) {
        Row(
            modifier = modifier
                .background(bubbleColor, shape)
                .padding(10.dp)
        ) {
            Text(
                text = text,
                color = contentColor,
                style = MaterialTheme.typography.body1,
            )
        }
    }

}

@Preview
@Composable
fun MessageBubblePreview() {
    Column {
        MessageBubble(text = "Hello!", isAuthor = false, hasTail = true)
        MessageBubble(text = "Hello there!", isAuthor = true)
    }
}